import com.tora.BigDecimalsCalc;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestBigDecimals {
    public static BigDecimalsCalc bigDecimalsCalc;

    @Test
    public void sum() {
        List<BigDecimal> decimals = new ArrayList<>();
        for (long i = 0; i < 5000000; i++) {
            decimals.add(BigDecimal.valueOf(i));
        }
        bigDecimalsCalc = new BigDecimalsCalc(decimals);
        var sum = bigDecimalsCalc.sum();
        assertEquals(BigDecimal.valueOf(Long.parseLong("12499997500000")), sum);
    }

    @Test
    public void average() {
        List<BigDecimal> decimals = new ArrayList<>();
        for (long i = 0; i < 5000000; i++) {
            decimals.add(BigDecimal.valueOf(i));
        }
        bigDecimalsCalc = new BigDecimalsCalc(decimals);
        var avg = bigDecimalsCalc.average();
        assertEquals(BigDecimal.valueOf(2500000), avg);
    }

    @Test
    public void topTenBiggest() {
        List<BigDecimal> decimals = new ArrayList<>();
        for (long i = 0; i < 5000000; i++) {
            decimals.add(BigDecimal.valueOf(i));
        }
        bigDecimalsCalc = new BigDecimalsCalc(decimals);
        var top10 = bigDecimalsCalc.topTen();
        for (int i = 0; i < 10; i++) {
            assertEquals(BigDecimal.valueOf(4999999 - i), top10.get(i));
        }
    }
}
