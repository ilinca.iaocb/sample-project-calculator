package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalsCalc {

    List<BigDecimal> decimals;

    public BigDecimalsCalc(List<BigDecimal> decimals) {
        this.decimals = decimals;
    }

    public BigDecimal sum() {
        return decimals.stream().reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

    }

    public BigDecimal average() {
        return sum().divide(BigDecimal.valueOf(decimals.size()), RoundingMode.CEILING);

    }

    public List<BigDecimal> topTen() {
        return decimals.stream()
                .sorted(Comparator.reverseOrder())
                .limit(10)
                .collect(Collectors.toList());
    }

}
