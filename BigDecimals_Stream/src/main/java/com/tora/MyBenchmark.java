package com.tora;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public abstract class MyBenchmark {

    protected DoubleAsPrimitive doubleAsPrimitiveCalc;
    protected DoubleAsObject doubleAsObjectCalc;

    public abstract void doSetup();

    @Benchmark
    public void sumPrimitive(Blackhole blackhole) {
        var sum = doubleAsPrimitiveCalc.sum();
        blackhole.consume(sum);
    }

    @Benchmark
    public void avgPrimitive(Blackhole blackhole) {
        var avg = doubleAsPrimitiveCalc.average();
        blackhole.consume(avg);
    }

    @Benchmark
    public void topTenPrimitive(Blackhole blackhole) {
        var top10 = doubleAsPrimitiveCalc.topTen();
        blackhole.consume(top10);
    }


    @Benchmark
    public void sumObj(Blackhole blackhole) {
        var sum = doubleAsObjectCalc.sum();
        blackhole.consume(sum);
    }


    @Benchmark
    public void avgObj(Blackhole blackhole) {
        var avg = doubleAsObjectCalc.average();
        blackhole.consume(avg);
    }


    @Benchmark
    public void topTenObj(Blackhole blackhole) {
        var top10 = doubleAsObjectCalc.topTen();
        blackhole.consume(top10);
    }

}
