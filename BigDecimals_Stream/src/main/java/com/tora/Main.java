package com.tora;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var MAX = 7000000;
        List<Double> arrL = new ArrayList<>(MAX);
        for (int i = 0; i < MAX; i++) {
            arrL.add((double) i);
        }
        System.out.println("Done");
        DoubleAsObject calculator = new DoubleAsObject(arrL);
        System.out.println(calculator.sum());
        System.out.println(calculator.average());
        for (double v : calculator.topTen()) {
            System.out.println(v);
        }

    }
}