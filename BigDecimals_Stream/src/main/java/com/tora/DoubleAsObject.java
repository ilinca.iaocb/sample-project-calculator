package com.tora;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleAsObject {
    public List<Double> numbers;

    public DoubleAsObject(List<Double> numbers) {
        this.numbers = numbers;
    }

    public Double sum() {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public Double average() {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public List<Double> topTen() {
        return numbers.stream()
                .sorted(Comparator.reverseOrder())
                .limit(10)
                .collect(Collectors.toList());
    }

}
