package com.tora;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleAsPrimitive {
    public double[] numbers;

    public DoubleAsPrimitive(double[] numbers) {
        this.numbers = numbers;
    }


    public double sum() {
        return Arrays.stream(numbers).sum();
    }

    public double average() {
        return Arrays.stream(numbers).average().orElse(0.0);
    }

    public double[] topTen() {
        return Arrays.stream(numbers)
                .boxed()  // Convert to Double stream
                .sorted((a, b) -> Double.compare(b, a))
                .limit(10)
                .mapToDouble(Double::doubleValue)
                .toArray();
    }


}
