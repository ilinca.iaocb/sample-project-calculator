package com.tora;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class SortedBenchmark extends MyBenchmark {

    @Override
    @Setup(Level.Iteration)
    public void doSetup() {
        var nrElems = 1000000;
        double[] arr = new double[nrElems];
        List<Double> objArr = new ArrayList<>(nrElems);
        for (int i = 0; i < nrElems; i++) {
            arr[i] = i;
            objArr.add((double) i);
        }
        super.doubleAsPrimitiveCalc = new DoubleAsPrimitive(arr);
        doubleAsObjectCalc = new DoubleAsObject(objArr);
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(SortedBenchmark.class.getSimpleName())
                .warmupIterations(2)
                .measurementIterations(2)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
