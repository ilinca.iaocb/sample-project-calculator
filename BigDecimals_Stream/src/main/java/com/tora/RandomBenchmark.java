package com.tora;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class RandomBenchmark extends MyBenchmark{

    @Override
    @Setup(Level.Iteration)
    public void doSetup() {
        var nrElems = 1000000;
        double[] arr = new double[nrElems];
        List<Double> objArr = new ArrayList<>(nrElems);
        var random = new Random(System.currentTimeMillis());
        for (int i = 0; i < nrElems; i++) {
            var number = random.nextDouble() * 1000;
            arr[i] = number;
            objArr.add(number);
        }
        super.doubleAsPrimitiveCalc = new DoubleAsPrimitive(arr);
        doubleAsObjectCalc = new DoubleAsObject(objArr);

    }



    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(RandomBenchmark.class.getSimpleName())
                .warmupIterations(2)
                .measurementIterations(2)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
