package com.tora.Operators;

import com.tora.Operands.MyNumber;

public class Multiplication implements IOperators {

    MyNumber firstOperand;
    MyNumber secondOperand;

    public Multiplication(MyNumber firstOperand, MyNumber secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    @Override
    public MyNumber calculate() {
        return new MyNumber(firstOperand.getN() * secondOperand.getN());
    }
}
