package com.tora.Operators;

import com.tora.Operands.MyNumber;

public class Maximum implements IOperators {

    MyNumber firstOperand;
    MyNumber secondOperand;

    public Maximum(MyNumber firstOperand, MyNumber secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    @Override
    public MyNumber calculate() {
        if (firstOperand.getN() > secondOperand.getN()) {
            return firstOperand;
        } else {
            return secondOperand;
        }
    }
}
