package com.tora.Operators;

import com.tora.Operands.MyNumber;

public class SquareRoot implements IOperators {

    MyNumber operand;

    public SquareRoot(MyNumber operand) {
        this.operand = operand;
    }

    @Override
    public MyNumber calculate() {
        return new MyNumber(Math.sqrt(operand.getN()));
    }
}
