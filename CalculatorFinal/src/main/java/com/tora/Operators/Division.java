package com.tora.Operators;

import com.tora.CalculatorException;
import com.tora.Operands.MyNumber;

public class Division implements IOperators {

    MyNumber firstOperand;
    MyNumber secondOperand;

    public Division(MyNumber firstOperand, MyNumber secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    @Override
    public MyNumber calculate() throws CalculatorException {
        if (!(secondOperand.getN() == 0)) {
            return new MyNumber(firstOperand.getN() / secondOperand.getN());

        } else {
            throw new CalculatorException("Division by 0 is not accepted!");
        }
    }
}
