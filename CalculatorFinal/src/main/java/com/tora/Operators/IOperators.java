package com.tora.Operators;

import com.tora.CalculatorException;
import com.tora.Operands.MyNumber;

public interface IOperators {
     MyNumber calculate() throws CalculatorException;
}

