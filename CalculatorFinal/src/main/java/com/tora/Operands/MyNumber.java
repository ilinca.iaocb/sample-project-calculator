package com.tora.Operands;

import java.util.Objects;

public class MyNumber {
    private double n;

    public MyNumber(double n) {
        this.n = n;
    }

    public double getN() {
        return n;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyNumber myNumber = (MyNumber) o;
        return Double.compare(myNumber.n, n) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(n);
    }

    @Override
    public String toString() {
        return String.valueOf(n);
    }
}
