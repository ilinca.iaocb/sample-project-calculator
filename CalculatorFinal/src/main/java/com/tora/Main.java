package com.tora;

import com.tora.Operands.MyNumber;
import com.tora.Operators.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static List<String> unaryOperands = List.of("sqrt");
    public static List<String> minMaxOperands = List.of("min", "max");

    public static void unaryCalculator(MyNumber operand) {
        SquareRoot squareRoot = new SquareRoot(operand);
        System.out.println(squareRoot.calculate());

    }

    public static void minMaxCalculator(String operator, MyNumber firstOperand, MyNumber secondOperand) {
        switch (operator) {
            case "min" : {
                Minimum min = new Minimum(firstOperand, secondOperand);
                System.out.println(min.calculate());
                break;
            }
            case "max" : {
                Maximum max = new Maximum(firstOperand, secondOperand);
                System.out.println(max.calculate());
                break;
            }
            default : {
            }
        }
    }

    public static void binaryCalculator(MyNumber firstOperand, String operator, MyNumber secondOperand) {
        switch (operator) {
            case "+" : {
                Adition add = new Adition(firstOperand, secondOperand);
                System.out.println(add.calculate());
                break;
            }
            case "-" :{
                Substraction sub = new Substraction(firstOperand, secondOperand);
                System.out.println(sub.calculate());
                break;
            }
            case "*" : {
                Multiplication mult = new Multiplication(firstOperand, secondOperand);
                System.out.println(mult.calculate());
                break;
            }
            case "/" : {
                Division div = new Division(firstOperand, secondOperand);

                try {
                    System.out.println(div.calculate());
                } catch (CalculatorException e) {
                    System.out.println(e.getMessage());
                }
                break;
            }
            default : {
            }
        }
    }

    public static void main(String[] args) throws IOException {

        while (true) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(System.in));
            System.out.println("give2");
            // Reading data using readLine
            String firstValue = reader.readLine();
            if (Objects.equals(firstValue, "exit")) break;
            if (unaryOperands.contains(firstValue)) {
                String secondValue = reader.readLine();
                MyNumber secondOperand = new MyNumber(Double.parseDouble(secondValue));
                unaryCalculator(secondOperand);
            } else if (minMaxOperands.contains(firstValue)) {
                String secondValue = reader.readLine();
                String thirdValue = reader.readLine();
                MyNumber firstOperand = new MyNumber(Double.parseDouble(secondValue));
                MyNumber secondOperand = new MyNumber(Double.parseDouble(thirdValue));
                minMaxCalculator(firstValue, firstOperand, secondOperand);

            } else {
                MyNumber firstOperand = new MyNumber(Double.parseDouble(firstValue));
                String operator = reader.readLine();
                String secondValue = reader.readLine();
                MyNumber secondOperand = new MyNumber(Double.parseDouble(secondValue));
                binaryCalculator(firstOperand, operator, secondOperand);
            }
        }

    }

}