import com.tora.CalculatorException;
import com.tora.Operands.MyNumber;
import com.tora.Operators.*;
import org.junit.Test;


import java.util.Objects;

//test
public class TestCalculator {
    @Test
    public void addition(){
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Adition addition = new Adition(firstOperand, secondOperand);
        var res =addition.calculate();
        assert (res.getN()== 12);
    }

    @Test
    public void substraction(){
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Substraction substraction = new Substraction(firstOperand, secondOperand);
        var res =substraction.calculate();
        assert(res.getN()== 6);
    }

    @Test
    public void multiplication(){
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Multiplication multiplication = new Multiplication(firstOperand, secondOperand);
        var res =multiplication.calculate();
        assert(res.getN()==27);
    }

    @Test
    public void squareRoot(){
        MyNumber operand = new MyNumber(9);

        SquareRoot squareRoot = new SquareRoot(operand);
        var res =squareRoot.calculate();
        assert(res.getN()== 3);
    }

    @Test
    public void min(){
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Minimum minimum = new Minimum(firstOperand, secondOperand);
        var res =minimum.calculate();
        assert(res.getN()==3);
    }

    @Test
    public void max() {
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Maximum maximum = new Maximum(firstOperand, secondOperand);
        var res = maximum.calculate();
        assert(res.getN()== 9);
    }

    @Test
    public void divisionOK(){
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(3);

        Division division = new Division(firstOperand, secondOperand);
        MyNumber res = null;
        try {
            res = division.calculate();
            assert(res.getN()==3);
        } catch (CalculatorException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void divisionError() {
        MyNumber firstOperand = new MyNumber(9);
        MyNumber secondOperand = new MyNumber(0);

        Division division = new Division(firstOperand, secondOperand);
        try {
             division.calculate();
        } catch (CalculatorException e) {
            assert(Objects.equals(e.getMessage(), "Division by 0 is not accepted!"));

        }
    }





}
