package com.tora;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> list = new ArrayList<T>();

    public ArrayListBasedRepository() {

    }

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

    @Override
    public void clear() {
        list.clear();
    }
}

