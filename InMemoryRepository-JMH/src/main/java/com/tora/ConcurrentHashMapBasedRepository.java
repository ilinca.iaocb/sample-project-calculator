package com.tora;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private ConcurrentHashMap<T, Boolean> map = new ConcurrentHashMap<>();

    public ConcurrentHashMapBasedRepository() {

    }

    @Override
    public void add(T item) {
        map.put(item, true);
    }

    @Override
    public boolean contains(T element) {
        return map.containsKey(element);
    }

    @Override
    public void remove(T element) {
        map.remove(element);
    }

    @Override
    public void clear() {
        map.clear();
    }
}

