package com.tora;

import com.koloboke.collect.map.hash.HashObjObjMaps;
import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class KolobokeBasedRepository<T> implements InMemoryRepository<T> {
    private HashObjSet<T> set = HashObjSets.newMutableSet();
//    private HashObjSet<T> set = HashObjSets.newMutableSet();

    public KolobokeBasedRepository() {
//        // Initialize the Koloboke hash set
//        set = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
