package com.tora;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;

public class FastUtilObjectSetRepository<T> implements InMemoryRepository<T> {
    private ObjectSet<T> objectSet = new ObjectOpenHashSet<>();

    public FastUtilObjectSetRepository() {

    }

    @Override
    public void add(T item) {
        objectSet.add(item);
    }

    @Override
    public boolean contains(T element) {
        return objectSet.contains(element);
    }

    @Override
    public void remove(T element) {
        objectSet.remove(element);
    }

    @Override
    public void clear() {
        objectSet.clear();
    }
}
