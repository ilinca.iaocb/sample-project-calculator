package com.tora;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;

public class MutableSetEclipseBasedRepository<T> implements InMemoryRepository<T> {
    private MutableSet<T> set = Sets.mutable.empty();

    public MutableSetEclipseBasedRepository() {

    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }

    @Override
    public void clear() {
        set.clear();
    }
}

