package com.tora;

import gnu.trove.set.TIntSet;
import gnu.trove.set.hash.TIntHashSet;

public class TroveBasedRepository implements InMemoryRepository<Integer> {
    private TIntSet set = new TIntHashSet();

    public TroveBasedRepository() {
//        set = new TIntHashSet();
    }

    @Override
    public void add(Integer item) {
        set.add(item);
    }

    @Override
    public boolean contains(Integer element) {
        return set.contains(element);
    }

    @Override
    public void remove(Integer element) {
        set.remove(element);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
