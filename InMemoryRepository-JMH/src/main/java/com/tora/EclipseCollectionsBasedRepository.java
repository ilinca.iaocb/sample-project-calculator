package com.tora;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.factory.Lists;

public class EclipseCollectionsBasedRepository<T> implements InMemoryRepository<T> {
    private MutableList<T> list = Lists.mutable.empty();

    public EclipseCollectionsBasedRepository() {

    }

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
