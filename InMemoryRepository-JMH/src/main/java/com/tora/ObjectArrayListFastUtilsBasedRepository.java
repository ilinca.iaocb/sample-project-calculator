
package com.tora;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class ObjectArrayListFastUtilsBasedRepository<T> implements InMemoryRepository<T> {
    private ObjectArrayList<T> list = new ObjectArrayList<>();

    public ObjectArrayListFastUtilsBasedRepository() {
    }

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
